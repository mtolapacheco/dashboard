import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomFirstComponent } from './custom-first/custom-first.component';
import { CustomSecondComponent } from './custom-second/custom-second.component';
import { CustomMainComponent } from './custom-main/custom-main.component';
import { CustomRoutingModule } from './custom-routing.module';
import { CustomDetailsComponent } from './custom-details/custom-details.component';

@NgModule({
  declarations: [CustomFirstComponent, CustomSecondComponent, CustomMainComponent, CustomDetailsComponent],
  imports: [
    CommonModule,
    CustomRoutingModule
  ]
})
export class CustomModule { }
